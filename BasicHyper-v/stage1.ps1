param($variables, $serverChoice)

# Import variables dot sourced
. $variables

# Install the Hyper-V hypervisor and all tools and download ISO for Ubuntu
Install-WindowsFeature -Name Hyper-V -IncludeManagementTools
$url = "https://releases.ubuntu.com/20.04.2.0/ubuntu-20.04.2.0-desktop-amd64.iso?_ga=2.152844345.1679593987.1619603442-484461018.1619603442"
$output = "C:\Users\dbruun\Downloads\ubuntu.iso"

$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)

#Create Hyper-V VMs
$VMName = $K8sName
$adapterName = $(Get-NetAdapter -InterfaceDescription "Microsoft Hyper-V*").name
$switch = $k8sSwitch
$VirtualMachine = "C:\hyperv\Virtual Machines"
$VirtualHardDisk = "C:\hyperv\Virtual Harddisk"
$InstallMedia = $(Resolve-Path ~\Downloads\ubuntu*).Path

# K8s specific params
$cpu = 2
$ramMaster = 2GB
$ramWorker = 4GB

# Standard
$mem = $ramMaster
$hdd = 40GB

# Create Virtual Switch
New-VMSwitch -name $switch -NetAdapterName $adapterName -AllowManagementOS $true

# Create New Virtual Machine
New-VM -Name $VMName -MemoryStartupBytes $mem -Generation 2 -NewVHDPath "$VirtualHardDisk\$VMName.vhdx" -NewVHDSizeBytes $hdd -Path "$VirtualMachine" -SwitchName $Switch
Set-VMProcessor -VMName $K8sName -Count $cpu

# Add DVD Drive to Virtual Machine
Add-VMScsiController -VMName $VMName
Add-VMDvdDrive -VMName $VMName -ControllerNumber 1 -ControllerLocation 0 -Path $InstallMedia

# Mount Installation Media
$DVDDrive = Get-VMDvdDrive -VMName $VMName

# Configure Virtual Machine to Boot from DVD and enable secureboot with template type for Linux
Set-VMFirmware -VMName $VMName -FirstBootDevice $DVDDrive -EnableSecureBoot On -SecureBootTemplate OpenSourceShieldedVM

Restart-Computer
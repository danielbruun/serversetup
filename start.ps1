#Start direkte fra Bitbucket.
#Baseline Server2019

#Invoke-Expression -Command $((Invoke-Webrequest https://bitbucket.org/danielbruun/ServerSetup/raw/HEAD/start.ps1).Content)

#Import variables
Invoke-Expression -Command $((Invoke-WebRequest https://bitbucket.org/danielbruun/ServerSetup/raw/HEAD/variables.ps1).Content)

# Choose provision type
$count = 1
foreach ($item in $choices) {
    $choice = ($item)
    Write-Output "$count) $choice"
    $count++
}
$serverNumber=Read-Host "Enter type of provisioning"
$serverNumber = ($serverNumber - 1)
$serverChoice = $choices[$serverNumber]

#Input credentials
# $credential = Get-Credential -Message 'Enter the local administrative username and password' -UserName $env:USERNAME

#Credentials to textfile for later use
# $credential.UserName | Out-File -FilePath $credfile
# ConvertFrom-SecureString $credential.Password | Out-File -FilePath $credfile -Append

# Get username and password from file, and create Credential-object
# $Username = (Get-Content $credfile)[0]
# $Password = (Get-Content $credfile)[-1] | ConvertTo-SecureString
# $credential = New-Object System.Management.Automation.PSCredential($Username, $Password)

#Install chocolatey + apps
Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco install git -y
choco install vscode -y
choco install firefox -y
choco install bitwarden -y

# Må kjøres med full path uten restart etter installasjon
Set-Location $rootDir
& "C:\Program Files\Git\cmd\git.exe" clone $repo

# Set resolution
Set-DisplayResolution -Width 1920 -Height 1080 -Force

# Remove Internet Explorer and remove startup task for server manager
Disable-WindowsOptionalFeature -FeatureName Internet-Explorer-Optional-amd64 -Online -NoRestart
Get-ScheduledTask -TaskName Servermanager | Disable-ScheduledTask
     
# Set keyboard layout
Set-WinUserLanguageList -LanguageList nb -Force

# Run stage 1 for provisioning choice
& "$path\$serverChoice\stage1.ps1" -variables $variables -serverChoice $serverChoice


param($variables, $serverChoice)

# Import variables dot sourced
. $variables

#Download Workstation
$url = "https://download3.vmware.com/software/player/file/VMware-player-16.1.0-17198959.exe?HashKey=1bbcad7cc4712fce2c5b8d1d8493aaa2&ext=.exe&params=%7B%22sourcefilesize%22%3A%22215.38+MB%22%2C%22dlgcode%22%3A%222a1acefeb1c0095b47f49060e2777f9d%22%2C%22languagecode%22%3A%22en%22%2C%22source%22%3A%22DOWNLOADS%22%2C%22downloadtype%22%3A%22manual%22%2C%22downloaduuid%22%3A%22143fb0f0-38d8-403a-8ba1-d7537882aab7%22%2C%22dlgtype%22%3A%22Product+Binaries%22%2C%22productversion%22%3A%2216.1.0%22%2C%22productfamily%22%3A%22VMware+Workstation+Player%22%7D&AuthKey=1607858664_bda28140974c9f028ad3da5822534e7d&ext=.exe"
$output = "C:\Users\dbruun\Downloads\test.exe"

$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
#OR
#(New-Object System.Net.WebClient).DownloadFile($url, $output)

#Remove saved credentials
Remove-Item -Path $credFile

#Install Workstation Player
& cmd /c "C:\Users\dbruun\Downloads\test.exe /s /v/qn EULAS_AGREED=1 AUTOSOFTWAREUPDATE=0 DATACOLLECTION=0 ADDLOCAL=ALL REBOOT=ReallySuppress"

Restart-Computer
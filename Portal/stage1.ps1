param($variables, $serverChoice)

# Import variables dot sourced
. $variables

#Clone terrazure repo
& "C:\Program Files\Git\cmd\git.exe" clone $terrazure


# Azure powershell and prereqs
# Dotnet framework
install-packageprovider -name nuget -force
choco install dotnetfx -y
choco install terraform -y

# Install Az CLI
Invoke-WebRequest -Uri https://aka.ms/installazurecliwindows -OutFile .\AzureCLI.msi; Start-Process msiexec.exe -Wait -ArgumentList '/I AzureCLI.msi /quiet'; rm .\AzureCLI.msi

Restart-Computer
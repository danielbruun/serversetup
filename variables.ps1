#Variables

#Start
$repo = "https://danielbruun@bitbucket.org/danielbruun/ServerSetup.git"
$terrazure = "https://danielbruun@bitbucket.org/danielbruun/terrazure.git"
$rootDir= "C:\"
$path = $rootDir + "ServerSetup"
$variables = "$path\variables.ps1"
$credFile = "C:\credfile.txt"
$choices = @("Basic","BasicHyper-v","Portal","Workstation")
$serverChoice = "Basic" #Default value

# Server
$nextScript = "$path\$serverChoice\stage1.ps1 -variables $variables -serverChoice $serverChoice"
$stage2 = "$path\$serverChoice\stage2.ps1"

# K8s
$K8sName = "Master"
$k8sSwitch = "K8s Switch"
